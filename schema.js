const { gql } = require('apollo-server-koa');

const typeDefs = gql`
  scalar Upload            

  type User {
    id: ID!
    email: String!
    password: String!
    paths: [Path!]
  }

  type Path {
    id: ID!
    filepath: String!
    createdAt: String!
    user: User!
  }

  type Query {
    paths: [Path!]
  }

  type Mutation {
    createResume(file: Upload!, userId: Int!): Path!
  }
`;

module.exports = typeDefs;
