const Koa = require('koa');
const bodyparser = require('koa-bodyparser');
const cors = require('@koa/cors');
const { ApolloServer } = require('apollo-server-koa');
const { graphqlUploadKoa } = require('graphql-upload');
const resolvers = require('./resolvers');
const typeDefs = require('./schema');
const Router = require('@koa/router');
const path = require('path');
const fs = require('fs');
const { error } = require('console');

const router = new Router();

/* Server will be hosted at PORT 4000 */
const PORT = 4000;

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

const app = new Koa();

/* allowing all the origins by using CORS */

app.use(cors({ origin: '*' }));

/* Middleware for handling file uplaods */

app.use(graphqlUploadKoa()); 
app.use(bodyparser());
app.use(router.routes());
app.use(router.allowedMethods());

router.get('/', async (ctx) => {
  ctx.body = 'Hello Koa';
});

/*
  using koa-static middleware for checking if the file is already uploaded in the directory, 
  if does then it will directly serve the file statically
*/
app.use(require('koa-static')(path.join(__dirname, 'uploads')));

try{
    async function startApolloServer() {
        await server.start();
        server.applyMiddleware({ app });
      }
      
      startApolloServer().then(() => {
        app.listen(PORT, () => {
          console.log(`Server listening at ${PORT}`);
        });
      });
}catch(error){
    console.log(error);
}

