const { PrismaClient } = require('@prisma/client');
const fs = require('fs');
const path = require('path');
const { finished } = require('stream/promises');

const prisma = new PrismaClient();

const resolvers = {
  Query: {

    /* Querying paths of the uploaded user by client-side */
    
    paths: async () => {
      try {
        return await prisma.path.findMany({
          include: { user: true },
        });
      } catch (error) {
        console.error('Error fetching paths:', error);
        throw new Error('Error fetching paths');
      }
    },
  },

  Mutation: {

    /* uploading resume mutation or creating file path 
    for storing it iin the database */

    createResume: async (_, { file, userId }) => {
      try {

        //using filesystem for reading and writing operations

        const { createReadStream, filename } = await file;    
        const stream = createReadStream();
        const filePath = path.join(__dirname, 'uploads', filename);
        
        // Ensure the uploads directory exists

        if (!fs.existsSync(path.join(__dirname, 'uploads'))) {
          fs.mkdirSync(path.join(__dirname, 'uploads'));
        }
        
        const out = fs.createWriteStream(filePath);
        stream.pipe(out);
        await finished(out);

         // create tables in database with parameters userid and filepath .

        const resumePath = await prisma.path.create({           
          data: {
            userID: userId,
            filepath: filePath,
          },
          include: { user: true },
        });

        return resumePath;
      } catch (error) {
        console.error('Error creating resume:', error);
        throw new Error('Error creating resume');
      }
    },
  },
};

module.exports = resolvers;
